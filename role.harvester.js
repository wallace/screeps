var roleHarvester = {

  distributing: function(creep) {
    return(creep.memory.distributing);
  },
  harvesting: function(creep) { 
    return(creep.memory.harvesting);
  },
  full: function(creep) {
    return(creep.carry.energy == creep.carryCapacity);
  },
  empty: function(creep) {
    return(creep.carry.energy == 0);
  },
  has_energy: function(creep) {
    return(creep.carry.energy > 0);
  }, 
  start_distributing: function(creep) {
    creep.memory.distributing = true;
    creep.memory.harvesting = false;
  },
  start_harvesting: function(creep) {
    creep.memory.distributing = false;
    creep.memory.harvesting = true;
  },
  /** @param {Creep} creep **/
  run: function(creep) {

    if(this.empty(creep)) {
      this.start_harvesting(creep);
    }

    if(this.full(creep)) {
      this.start_distributing(creep);
    }

    if(this.distributing(creep)) {
      var targets = creep.room.find(FIND_STRUCTURES, {
        filter: (structure) => {
          return (structure.structureType == STRUCTURE_EXTENSION ||
                  structure.structureType == STRUCTURE_SPAWN ||
                  structure.structureType == STRUCTURE_TOWER) && structure.energy < structure.energyCapacity;
        }
      });
      var containers = creep.room.find(FIND_STRUCTURES, {
        filter: (structure) => {
          var total = _.sum(structure.store);
          return (structure.structureType == STRUCTURE_CONTAINER &&
                  total < structure.storeCapacity);
        }
      });

      if(targets.length > 0 || containers.length > 0) {
        if(creep.transfer(targets[0], RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
          creep.moveTo(targets[0]);
        }

        if(creep.transfer(containers[0], RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
          creep.moveTo(containers[0]);
        }
      } else {
        console.log('bored harvester; pretends to be upgrader');

        if(creep.upgradeController(creep.room.controller) == ERR_NOT_IN_RANGE) {
          creep.moveTo(creep.room.controller);
        }
      }
    }

    if(this.harvesting(creep)) {
      var sources = creep.room.find(FIND_SOURCES);
      if(creep.harvest(sources[0]) == ERR_NOT_IN_RANGE) {
        creep.moveTo(sources[0]);
      }
    }
  }
};

module.exports = roleHarvester;
