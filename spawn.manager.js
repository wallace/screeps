var spawnManager = {
  run: function(creeps) {
    var harvesters;
    var upgraders;
    var builders;
    var repairers;

    var partitioned_creeps = this.partition_into_role(creeps);
    harvesters = partitioned_creeps['harvesters'];
    upgraders = partitioned_creeps.upgraders;
    builders = partitioned_creeps.builders;
    repairers = partitioned_creeps.repairers;

    console.log('harvesters: ' + harvesters.length);
    console.log('builders: ' + builders.length);
    console.log('upgraders: ' + upgraders.length);
    console.log('repairers: ' + repairers.length);

    if(repairers.length < 3) {
      Game.spawns.adam.createCreep( [WORK, WORK, WORK, WORK, CARRY, CARRY, CARRY, CARRY, MOVE, MOVE, MOVE, MOVE], null, { 'role': 'repairer' } );
    }

    if(upgraders.length < 2) {
      Game.spawns.adam.createCreep( [WORK, WORK, WORK, WORK, CARRY, CARRY, CARRY, CARRY, MOVE, MOVE, MOVE, MOVE], null, { 'role': 'upgrader' } );
    }

    if(builders.length < 4) {
      Game.spawns.adam.createCreep( [WORK, WORK, WORK, WORK, CARRY, CARRY, CARRY, CARRY, MOVE, MOVE, MOVE, MOVE], null, { 'role': 'builder' } );
    }

    if(harvesters.length < 3) {
      Game.spawns.adam.createCreep( [WORK, WORK, WORK, WORK, CARRY, CARRY, CARRY, CARRY, MOVE, MOVE, MOVE, MOVE], null, { 'role': 'harvester' } );
    }

  },
  partition_into_role: function(creeps) {
    var repairers = [];
    var upgraders = [];
    var builders = [];
    var harvesters = [];

    for(var name in creeps) {
      var creep = Game.creeps[name];
      switch(creep.memory.role) {
        case 'repairer':
          repairers.push(creep);
          break;
        case 'upgrader':
          upgraders.push(creep);
          break;
        case 'builder':
          builders.push(creep);
          break;
        case 'harvester':
          harvesters.push(creep);
          break;
        default:
          console.log('uh oh, unknown role');
      }
    }
    return {
      'repairers': repairers,
      'upgraders': upgraders,
      'builders': builders,
      'harvesters': harvesters
    };
  }
};

module.exports = spawnManager;
