var worldManager = {
  run: function(creep) {
    var roads;
    var containers;
    var walls;
    var ramparts;
    var construction_sites;

    ramparts = this.needy_ramparts();
    console.log('ramparts('+ramparts.length+'): ' + _.map(ramparts, function(e) { return e.hits; }));

    construction_sites = this.needy_construction_sites();
    console.log('construction_sites('+construction_sites.length+'): ' + _.map(construction_sites, function(e) { return e.progress; }));

    roads = this.needy_roads(creep);
    console.log('roads('+roads.length+'): ' + _.map(roads, function(e) { return e.hits; }));
    if(Memory.road_check < 0 || Memory.road_check === undefined) {
      Memory.road_check = 2;

      roads = this.needy_roads(creep);
      Memory.roads = _.map(roads, function(e) { return e.id });
    } else {
      roads = Memory.roads;
    }
    Memory.road_check = Memory.road_check - 1

    containers = this.needy_containers(creep);
    console.log('containers('+containers.length+'): ' + _.map(containers, function(e) { return e.hits; }));

    walls = this.needy_walls(creep);
    console.log('walls('+walls.length+'): ' + _.map(walls, function(e) { return e.hits; }));

    return {
      'ramparts': ramparts,
      'construction_sites': construction_sites,
      'roads': roads,
      'containers': containers,
      'walls': walls
    };
  },
  needy_roads: function(creep) {
    var roads = creep.room.find(FIND_STRUCTURES, {
      filter: (structure) => {
        if(structure.structureType != STRUCTURE_ROAD) {
          return false;
        }
        return (structure.hits < 5000);
      }
    });
    roads = _.sortBy(roads, function(e) { return e.hits });

    return roads;
  },
  needy_containers: function(creep) {
    var containers = creep.room.find(FIND_STRUCTURES, {
      filter: (structure) => {
        if(structure.structureType != STRUCTURE_CONTAINER) {
          return false;
        }
        return (structure.hits <= (structure.hitsMax - 50000));
      }
    });

    return containers;
  },
  needy_walls: function(creep) {
    var walls = creep.room.find(FIND_STRUCTURES, {
      filter: (structure) => {
        if(structure.structureType != STRUCTURE_WALL) {
          return false;
        }
        return (structure.hits <= structure.hitsMax / 3000);
      }
    });

    return walls;
  },
  needy_ramparts: function() {
    var ramparts = _.filter(Game.structures, function(structure) {
      if(structure.structureType != STRUCTURE_RAMPART) {
        return false;
      }
      return (structure.hits < 75000);
    });
    //ramparts = _.sortBy(ramparts, function(e) { return e.hits });
    return ramparts;
  },
  needy_construction_sites: function() {
    var construction_sites = _.filter(Game.constructionSites, function(site) {
      return (site.progress < site.progressTotal);
    });
    return construction_sites;
  }
};

module.exports = worldManager;
