var roleRepairer = {
  harvesting: function(creep) { 
    return(creep.memory.harvesting);
  },
  repairing: function(creep) {
    return(creep.memory.repairing);
  },
  full: function(creep) {
    return(creep.carry.energy == creep.carryCapacity);
  },
  empty: function(creep) {
    return(creep.carry.energy == 0);
  },
  has_energy: function(creep) {
    return(creep.carry.energy > 0);
  }, 
  start_repairing: function(creep) {
    creep.memory.repairing = true;
    creep.memory.harvesting = false;
  },
  start_harvesting: function(creep) {
    creep.memory.repairing = false;
    creep.memory.harvesting = true;
  },
  /** @param {Creep} creep **/
  run: function(creep, results) {
    var roads = results['roads'];
    var containers = results['containers'];

    if(this.empty(creep)) {
      this.start_harvesting(creep);
    }

    if(this.full(creep)) {
      this.start_repairing(creep);
    }

    var roads = creep.room.find(FIND_STRUCTURES, {
      filter: (road) => {
        return (road.id === roads[0]);
      }
    });
    console.log('road[' + roads[0].id + ']: ' + roads[0].hits);
    if (roads[0].hits === 5000) {
      _.slice(Memory.roads, 1);
    }

    if(this.repairing(creep)) {
      if(containers.length > 0) {
        if(creep.repair(containers[0]) == ERR_NOT_IN_RANGE) {
          creep.moveTo(containers[0]);
        }
      } else if(roads.length > 0) {
        if(creep.repair(roads[0]) == ERR_NOT_IN_RANGE) {
          console.log('repairing roads');
          creep.moveTo(roads[0]);
        }
      } else if(walls.length > 0) {
        if(creep.repair(walls[0]) == ERR_NOT_IN_RANGE) {
          creep.moveTo(walls[0]);
        }
      } else {
        // act like an upgrader
        if(creep.upgradeController(creep.room.controller) == ERR_NOT_IN_RANGE) {
          creep.moveTo(creep.room.controller);
        }
      };
    }

    if(this.harvesting(creep)) {
      var sources = creep.room.find(FIND_SOURCES);
      if(creep.harvest(sources[1]) == ERR_NOT_IN_RANGE) {
        creep.moveTo(sources[1]);
      }
    }
  }
};

module.exports = roleRepairer;
