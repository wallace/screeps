var roleBuilder = {
  harvesting: function(creep) { 
    return(creep.memory.harvesting);
  },
  building: function(creep) {
    return(creep.memory.building);
  },
  full: function(creep) {
    return(creep.carry.energy == creep.carryCapacity);
  },
  empty: function(creep) {
    return(creep.carry.energy == 0);
  },
  has_energy: function(creep) {
    return(creep.carry.energy > 0);
  }, 
  start_building: function(creep) {
    creep.memory.building = true;
    creep.memory.harvesting = false;
  },
  start_harvesting: function(creep) {
    creep.memory.building = false;
    creep.memory.harvesting = true;
  },

  /** @param {Creep} creep **/
  run: function(creep, results) {

    if(this.empty(creep)) {
      this.start_harvesting(creep);
    }

    if(this.full(creep)) {
      this.start_building(creep);
    }

    if(this.building(creep)) {
      var ramparts = results['ramparts'];
      var targets = results['construction_sites'];
      var walls = results['walls'];

      if(ramparts.length > 0) {
        if(creep.repair(ramparts[0]) == ERR_NOT_IN_RANGE) {
          creep.moveTo(ramparts[0]);
        }
      } else if(targets.length) {
        if(creep.build(targets[0]) == ERR_NOT_IN_RANGE) {
          creep.moveTo(targets[0]);
        }
      } else if(walls.length) {
        if(creep.repair(walls[0]) == ERR_NOT_IN_RANGE) {
          creep.moveTo(walls[0]);
        }
      }
    }

    if(this.harvesting(creep)) {
      var sources = creep.room.find(FIND_SOURCES);
      if(creep.harvest(sources[1]) == ERR_NOT_IN_RANGE) {
        creep.moveTo(sources[1]);
      }
    }
  }
};

module.exports = roleBuilder;
